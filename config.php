<?php

$db_path = "/var/www/fcvnci/api/fcvnci.db"; //Path to the sqlite database - Please move out of the web directory

$timeout_period = 60; //Period of time (in seconds) before sessions are automatically removed
$debugging = true; //Enable debugging mode?
$salt = "SpeedSchoolRocksMySocks"; //Salt for sessionkey - Change on every server FCVNC(I) is deployed on

//Start Instructor Station Settigns
$enableInstructorStation = true; //Is there a dedicated instructor station running a vnc server? Should the following settings apply?
$InstructorStationIP = "136.165.219.104"; //IP of the instructor station - For VNC server and allowing into control panel without auth
$InstructorStationPort = 5900; //Port of the VNC server on the instructor station
$InstructorStationPassword = "$3@a^V9!"; //8 characer VNC password for Instructor station
$showInstructorStationDefault = true; //Should student stations default to showing the instructor station if students aren't presenting
//End Instructor Station Settigns

$fcvncStations = array("136.165.45.162", "136.165.199.77", "136.165.201.149"); //Array of FCVNC student stations the server needs to communicate with
$fcvncStationAPIPath = "/fcvnc/api/"; //Web path to the api folder on the FCVNC student stations
?>
