var heartbeatTimer = setInterval(function() {heartbeat()}, 10000);
var userListTimer = setInterval(function() {updateUsers("#user-list")}, 5000);
	$(document).ready(updateUsers("#user-list"));
var stationListTimer = setInterval(function() {updateStations("#station-list")}, 15000);
	$(document).ready(updateStations("#station-list"));
var FCVNCPollTimer = setInterval(function() {fcvncPoll()}, 7000);
	$(document).ready(fcvncPoll());

function fcvncPoll() {
	$.ajax({
                type: "GET",
                url: "api/poll-FCVNCs.php"
        });
}

function logout() {
	$.getJSON("api/destroy-session.php", function(data) {
		if(data == "Success") {
			window.location.href = "index.php";
		}
		else {
			alert("Failed to log out...");
		}
	});
}

function saveLocalVNCSettings(portField, passwordField) {
	$.ajax({
		type: "POST",
		url: "api/set-vnc-settings.php",
		data: { port: $(portField).val(), password: $(passwordField).val()}
	});
}

function heartbeat() {
	$.getJSON("api/heartbeat.php", function(data) {
		if(data[0] == "Failed") {
			clearInterval(heartbeatTimer);
			alert(data[1]);
			window.location.href = "index.php";
		}
	});
}

function updateUsers(userlist) {
	var lastSelect = $(userlist).val(); //Last selected select item
	$.getJSON("api/user-list.php", function(data) {
		var options = "";
		var lastStation = "";
		for(var i = 0; i < data.length; i++) {
			//If the user about to be added is from the same station as the last, add option, else add new option group and add user
			if(lastStation == data[i].stationHostname) {
				lastStation = data[i].stationHostname;
				//If user to be added is the same as the user selected, add selected tag, else don't
				if(data[i].id == lastSelect)
					options += '<option value="' + data[i].id + '" selected>' + data[i].user + '</option>';
				else
					options += '<option value="' + data[i].id + '">' + data[i].user + '</option>';
			}
			else {
				lastStation = data[i].stationHostname;
				options += '<optgroup label="' + data[i].stationHostname + '">';
				//If user to be added is the same as the user selected, add selected tag, else don't
				if(data[i].id == lastSelect)
					options += '<option value="' + data[i].id + '" selected>' + data[i].user + '</option>';
				else
					options += '<option value="' + data[i].id + '">' + data[i].user + '</option>';
				//Add a closing optgroup tag if the next users station is different or there is no next user
				if(i+1 < data.length) {
					if(data[i+1].stationHostname != data[i].stationHostname)
						options += "</optgroup>";
				}
				else {
					options += "</optgroup>";
				}
			}
		}
		$(userlist).html(options);
	});
} 

function updateStations(stationlist) {
	var lastSelect = $(stationlist).val(); //Last selected select item
	$.getJSON("api/station-list.php", function(data) {
		var options = "";
		for(var i = 0; i < data.length; i++) {
			//If user to be added is the same as the station selected, add selected tag, else don't
			if(data[i].id == lastSelect)
				options += '<option value="' + data[i].id + '" selected>' + data[i].hostname + '</option>';
			else
				options += '<option value="' + data[i].id + '">' + data[i].hostname + '</option>';
		}
		$(stationlist).html(options);
	});
} 

//function present() {
//	$.getJSON("api/start-present.php", function(data) {
//		if(data != "Success") {
//			alert("Failed to present: " + data);
//		}
//	});
//}

//function stopPresent() {
//	$.getJSON("api/stop-present.php", function(data) {
//		if(data != "Success") {
//			alert("Failed to stop presenting: " + data);		}
//	});
//}


