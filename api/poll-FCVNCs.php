<?php
//The script polls the FCVNC servers listed in config.php and populates the database with the information
header('Content-Type: application/json');

//Only allow one instance of this script to run
$fp = fopen('/tmp/fcvnci-poll.lock', 'c+');
if(!flock($fp, LOCK_EX | LOCK_NB)) {
	die(json_encode("Script already running! Only 1 instance allowed"));
}

include(realpath(dirname(__FILE__)) . "/../config.php");
//Include function to do simultaneous cURL requests
include("multirequest.php");

//Path to the get-users.php script on each server
$getUsers = $fcvncStations;
foreach($getUsers as &$value) {
	$value = $value . $fcvncStationAPIPath . "instructor/get-users.php";
}

//Path to the station-info.php script on each server
$getInfo = $fcvncStations;
foreach($getInfo as &$value) {
        $value = $value . $fcvncStationAPIPath . "instructor/station-info.php";
}

//Extra cURL options - Connection timeout
$options = array(CURLOPT_CONNECTTIMEOUT => 5);

//Make requests to FCVNC student stations
$results = multiRequest(array_merge($getInfo, $getUsers), $options);

//Populate DB. Array end 0-length of $fcvncstations are the hostnames.
//lenght of $fcvncstations to end are the users for teach station
try {
	$DBH = new PDO("sqlite:$db_path");
	if($debugging == true)
		$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging

	$time = time(); //Time var for updating time column and deleting entries that don't get updated/added
	//Add entries to each the correct tables
	for($i = 0; $i < count($fcvncStations); $i++) {
		//Check if return gave success
		if(json_decode($results[$i])[0] == "Success") {
			//Add to stations table
			$ip = $fcvncStations[$i];
			$host = json_decode($results[$i])[1];
			//Check if station exists by hostname
			$query = $DBH->prepare("SELECT * FROM stations WHERE hostname == :hostname");
			$query->bindParam(':hostname', $host);
			$query->execute();
			$row = $query->fetch(PDO::FETCH_NUM);
			if($row > 0) {
				//Station exists in DB, update time
				$DBH->exec("UPDATE stations SET time = " . $time . " WHERE id == " . $row[0]);
			}
			else {
				//Station not in DB, add to DB
				$query = $DBH->prepare("INSERT INTO stations (ip, hostname, time) VALUES (:ip, :hostname, :time)");
				$query->bindParam(':ip', $ip);
				$query->bindParam(':hostname', $host);
				$query->bindParam(':time', $time);
				$query->execute();
			}
			//Add stationUsers
			$users = json_decode($results[$i + count($fcvncStations)], true);
			foreach($users as $user) {
				//Check if user exists in DB by userID
				$query = $DBH->prepare("SELECT * FROM stationUsers WHERE userID == :id");
				$query->bindParam(':id', $user["id"]);
				$query->execute();
				$row = $query->fetch(PDO::FETCH_NUM);
				if($row > 0) {
					//User exists in DB, update time
					$DBH->exec("UPDATE stationUsers SET time = " . $time . " WHERE userID == " . $user["id"]);
				}
				else {
					//User not in DB, add to DB
					$query = $DBH->prepare("INSERT INTO stationUsers (userID, user, stationIP, stationHostname, time) VALUES (:ID, :user, :ip, :host, :time)");
					$query->bindParam(':ID', $user["id"]);
                	        	$query->bindParam(':user', $user["user"]);
					$query->bindParam(':ip', $ip);
					$query->bindParam(':host', $host);
					$query->bindParam(':time', $time);
                	        	$query->execute();
				}
			}
		}
	}
	//Delete stations and stationUsers that didn't get updated/created
	$DBH->exec("DELETE FROM stations WHERE time != " . $time);
	$DBH->exec("DELETE FROM stationUsers WHERE time != " . $time);
	$DBH = null;
}
catch(PDOException $e) {
	echo json_encode($e->getMessage());
}

//Close file lock
fclose($fp);

?>
