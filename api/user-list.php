<?php
//Returns list of all the users in FCVNCI database for display in interface
include(realpath(dirname(__FILE__)) . "/../config.php");

session_start();
header('Content-Type: application/json');
if(isset($_SESSION['sessionkey'])) {
	$DBH = new PDO("sqlite:$db_path");
	if($debugging == true)
		$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging

	//Query stationUsers && session for information
	$query = $DBH->query("SELECT 'i' || id AS id, user, stationHostname FROM session ORDER BY user ASC");
        $FCVNCIusers = $query->fetchALL(PDO::FETCH_ASSOC);
	$query = $DBH->query("SELECT id, user, stationHostname FROM stationUsers ORDER BY stationHostname ASC");
	$stationUsers = $query->fetchALL(PDO::FETCH_ASSOC);
	echo json_encode(array_merge($FCVNCIusers, $stationUsers));
}
else {
	echo json_encode("Not Authorized");
}

?>
