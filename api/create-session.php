<?php
//Create a session for the user and record it in the database

//Maintenance script call
exec("php maintenance.php");

function createSession($dbpath, $debugging, $user, $ip, $time, $salt) {
	//Generate the SessionKey
	$sessionkey = md5($salt . $_SESSION['user'] . time() . $ip);
	try {
		$DBH = new PDO("sqlite:$dbpath");
		if($debugging == true)
			$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging

		//Check if sessionkey already exists in DB. If so, update time, else, create session.
		$query = $DBH->prepare("SELECT * FROM session WHERE sessionkey = :sessionkey");
		$query->bindParam(':sessionkey', $sessionkey);
		$query->execute();
		$row = $query->fetch(PDO::FETCH_NUM);
		if($row > 0) {
			//Sessionkey exists, update time
			$query = $DBH->query("UPDATE session SET time = " . $time . " WHERE id == " . $row[0]);
		}
		else {
			//Sessionkey does not exist, Create new session in DB
			$query = $DBH->prepare("INSERT INTO session (user, time, sessionkey, ip) VALUES (:user, :time, :sessionkey, :ip)");
			$data = array( 'user' => $user, 'time' => $time, 'sessionkey' => $sessionkey, 'ip' => $ip );
			$query->execute($data);
		}
		$DBH = null;
		return $sessionkey;
	}
	catch(PDOException $e) {
		echo $e->getMessage();
	}
}
?>
