<?php
//Handler for index.php form

include(realpath(dirname(__FILE__)) . "/config.php"); //Pull in $salt
include(realpath(dirname(__FILE__)) . "/api/create-session.php"); //Pull in createSession function

//Remove any existing PHP Session
session_start();
session_destroy();

//Get data for session
session_start();
$_SESSION['user'] = htmlspecialchars($_GET['user']);
$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
$_SESSION['time'] = time();
$_SESSION['sessionkey'] = createSession($db_path, $debugging, $_SESSION['user'], $_SESSION['ip'], $_SESSION['time'], $salt);

//Forward to control page
header('Location: ./control.php');

?>
