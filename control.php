<?php
//Main page for users. Contains list of users, current server status, and the VNC Server/Server Settigns

include(realpath(dirname(__FILE__)) . "/config.php"); //Pull in $db_path
//Maintenance script call
exec("php api/maintenance.php");

//Check if session exists (both Cookie and in DB). Redirect to login if session doesn't exist.
session_start();
if(isset($_SESSION['sessionkey'])) {
	try {
		$DBH = new PDO("sqlite:$db_path");
		$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging
		$query = $DBH->prepare("SELECT COUNT(id) FROM session WHERE sessionkey = :sessionkey");
		$query->bindParam(':sessionkey', $_SESSION['sessionkey']);
		$query->execute();
		$rows = $query->fetch(PDO::FETCH_NUM);
		if($rows['0'] == 0) {
			//Sessionkey not in DB, but has cookie. Delete cookie and force re-login
			session_destroy();
			header('Location: ./index.php');
		}
		$DBH = null;
	}
	catch(PDOException $e) {
		echo $e->getMessage();
	}
}
else {
	session_destroy();
	header('Location: ./index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title>Flipped Classroom TV Collaboration Instructor - <?php echo gethostname(); ?></title>
<!--[if lt IE 9]>
	<script src="./assets/javascripts/html5.js"></script>
<![endif]-->
	<link rel="stylesheet" href="./assets/stylesheets/style.css" />
<!--[if (gt IE 8) | (IEMobile)]><!-->
	<link rel="stylesheet" href="./assets/stylesheets/unsemantic-grid-responsive.css" />
<!--<![endif]-->
<!--[if (lt IE 9) & (!IEMobile)]>
	<link rel="stylesheet" href="./assets/stylesheets/ie.css" />
<![endif]-->
<script type="text/javascript" src="assets/javascripts/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="assets/javascripts/control.js"></script>
<script type="text/javascript" src="assets/javascripts/tabs.js"></script>
</head>
<body>
  <div class="grid-container">
    <?php include('header.php'); ?>
	<div class="grid-100 mobile-grid-100">
		<h3>Classroom Settings</h3>
		<section class="grey-box-wrapper">
			[CHECK] Allow students to present at student stations<br/>
			[CHECK] Show <select id="default-present"><option value="0">Nothing</option><option value="1" selected>Instructor Station</option></select> if students are not presenting at student stations
		</section>
	</div>
	<div class="grid-100 mobile-grid-100">
                <h3>Current Status</h3>
                <section class="grey-box-wrapper">
                        <div class="presenting-status">Default Behavior (Showing instructor station if students are not presenting)</div>
                </section>
        </div>
    	<div class="grid-33 mobile-grid-100">
    		<section class="user-list-block">
				<h3>User Select</h3>
				<div class="user-list-content">
					<select id="user-list" size="2"></select><br />
					<button type="button" style="width:100%;" onclick="logout()" <?php if($_SERVER['REMOTE_ADDR'] == $InstructorStationIP && $enableInstructorStation == true) echo "disabled"?>>Logout</button>
				</div>
      		</section>
    	</div>
    <div class="grid-33 mobile-grid-100">
    	<section class="available-stations-block">
			<h3>Station Select</h3>
			<div class="station-list-content">
				<select id="station-list" size="2" multiple></select><br />
				<button type="button" style="width:49%;" onclick="present()">Present</button>
				<button type="button" style="width:49%; float:right;" onclick="stopPresent()">Stop Presenting</button>
			</div>
    	</section>
    </div>
    <div class="grid-33 mobile-grid-100">
      <section class="control-block">
	<h3>VNC Server</h3>
	<div id="tabContainer">
		<div class="tabs">
			<ul>
				<?php if($_SERVER['REMOTE_ADDR'] == $InstructorStationIP && $enableInstructorStation == true) { ?>
				<li id="tabHeader_1">Instructor Station VNC Server</li>
				<?php } else { ?>
				<li id="tabHeader_1">Java VNC Server (Slower)</li>
				<li id="tabHeader_2">Local VNC Server (Faster)</li>
				<?php }; ?>
			</ul>
		</div>
		<div class="tabscontent">
			<?php if($_SERVER['REMOTE_ADDR'] == $InstructorStationIP && $enableInstructorStation == true) { ?>
			<div class="tabpage" id="tabpage_1">
				<table border="0" cellpadding="1" cellspacing="1" style="width: 100%;">
					<tbody>
						<tr>
							<td style="width: 65px;">Port:</td><td><input type="number" id="vncport" name="port" min="1" max="65535" value="<?php echo $InstructorStationPort; ?>" style="width: 100%" disabled></td>
						</tr>
						<tr>
							<td style="width: 65px;">Password:</td><td><input type="password" id="vncpassword" name="vncpassword" style="width: 100%;" value="Nice Try..." disabled></td>
						</tr>
						<tr>
							<td colspan="2" style="text-align:center;"><input type="button" value="Save" style="width:100%;" onclick="alert('These settings cannot be changed for the Instructor Station. Please contact the Lab Administrator for help.')" disabled></td>
						</tr>
					</tbody>
				</table>
			</div>
			<?php } else { ?>
			<div class="tabpage" id="tabpage_1">
				<center>Please do not change the port or password below.</center>
				<applet code="GSVNCJ.class" archive="assets/freegsvncj.jar" width=350 height=250>
					<param name="port" value="5900">
					<param name="password" value="<?php echo $_SESSION['sessionkey']; ?>">
					<param name="autoStart" value="true">
					<param name="titleLabel" value="Java VNC Server">
					<param name="portLabel" value="Port">
					<param name="passwordLabel" value="Password">
					<param name="startLabel" value="Enable">
					<param name="stopLabel" value="Disable">
					<param name="mainBackground" value="CCCCCC">
					<param name="mainForeground" value="000">
					<param name="buttonBackground" value="8A2529">
					<param name="buttonForeground" value="FFFFFF">
					<param name="msgBackground" value="CCCCCC">
					<param name="msgForeground" value="000000">
					<param name="MSG1" value="Starting authenication {0}...">
					<param name="MSG2" value="{0} authentication successfull!">
					<param name="MSG3" value="{0} authentication failed!">
					<param name="MSG4" value="{0} can''t be opened!">
					<param name="MSG5" value="Exception: {0}">
					<param name="MSG6" value="Start listening on {0}...">
					<param name="MSG7" value="Client address: {0}">
					<param name="MSG8" value="shutting down {0} server VNCServer">
					<param name="MSG9" value="Client {0} closed">
				</applet>
			</div>
			<div class="tabpage" id="tabpage_2">
				<table border="0" cellpadding="1" cellspacing="1" style="width: 100%;">
					<tbody>
						<tr>
							<td style="width: 65px;">Port:</td><td><input type="number" id="vncport" name="port" min="1" max="65535" value="5900" style="width: 100%"></td>
						</tr>
						<tr>
							<td style="width: 65px;">Password:</td><td><input type="password" id="vncpassword" name="vncpassword" style="width: 100%;"></td>
						</tr>
						<tr>
							<td colspan="2" style="text-align:center;"><input type="button" value="Save" style="width:100%;" onclick="saveLocalVNCSettings('#vncport', '#vncpassword')"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<?php }; ?>
		</div>
	</div>
      </section>
    </div>
   <div class="grid-100">
        <h3>Push Content</h3>
        <section class="grey-box-wrapper">
                To Be Implemented
        </sction>
   </div>
   <div class="grid-100">
        <h3>Classroom Chat</h3>
        <section class="grey-box-wrapper">
                To Be Implemented
        </sction>
   </div>
  </div>
<?php include("footer.html"); ?>
</body>
</html>
