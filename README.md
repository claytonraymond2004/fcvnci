# FCVNCI - Flipped Classroom VNC TV Collaboration Instructor Console
This project is built to extend the functionality of the [FCVNC project](https://bitbucket.org/claytonraymond2004/fcvnc) to provide an instructor console. This console will allow the instructor to broadcast their screen to certain/all FCVNC student servers, share a specific student screen to all/certain FCVNC student servers, push presentations/videos/other media, to the FCVNC student servers for local playback (since VNC doesn't allow the broadcasting of video very well, this allows media to be played back smoothly)

Required Hardware:

* At least one FCVNC student instance
* Moderately powerful Debian server to act as a VNC Reflector (this allows instructor/student computers to be broadcast efficiently)
	* For developement, this server will also act as the server for this part of the project.

Required Software: 

* Apache + PHP + PDO + SQLite + cURL


Note: Project devloped and tested on Debian 7 LXDE


Tools Used:

* JQuery
* Unsemantic CSS Framework
* [freegsvncj.jar](http://gotoservers.com/gsvncj.html)
* [multirequest.php](http://www.phpied.com/simultaneuos-http-requests-in-php-with-curl/)


*** /var/fcvnc/ ***  
I set up a `/var/fcvnc` folder with correct permissions for the storage of private data.  
Make sure the `config.php` points to the correct path when doing this and that the permissions are correct.  
