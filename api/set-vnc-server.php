<?php
//Update database with Local VNC Server settings
include(realpath(dirname(__FILE__)) . "/../config.php"); //Pull in $db_path

header('Content-Type: application/json');

$source = $_POST['vncsource'];

session_start();
if(isset($_SESSION['sessionkey'])) {
        try {
                $DBH = new PDO("sqlite:$db_path");
                if($debugging == true)
                        $DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging

                //Switch vncsource - 0 for Java applet, 1 for local
                $query = $DBH->prepare("UPDATE session SET vncsource = :source");
                $query->bindParam(':source', $source);
                $query->execute();
                $DBH = null;
                echo json_encode("Success");
        }
        catch(PDOException $e) {
                echo $e->getMessage();
        }
}
?>
