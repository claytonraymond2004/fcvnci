<?php
//Keepalive called by clients. Update time in DB and session. Returns nothing
include(realpath(dirname(__FILE__)) . "/../config.php"); //Pull in $db_path
//Maintenance script call
exec("php maintenance.php");

header('Content-Type: application/json');
//Update time of DB entry for session
session_start();
if(isset($_SESSION['sessionkey'])) {
	$_SESSION['time'] = time();
	try {
		$DBH = new PDO("sqlite:$db_path");
		if($debugging == true)
			$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging

		//Check if sessionkey already exists in DB. If so, update time, else, delete cookie session
		$query = $DBH->prepare("SELECT * FROM session WHERE sessionkey = :sessionkey");
		$query->bindParam(':sessionkey', $_SESSION['sessionkey']);
		$query->execute();
		$row = $query->fetch(PDO::FETCH_NUM);
		if($row > 0) {
			//Sessionkey exists, update time and return success JSON
			$query = $DBH->query("UPDATE session SET time = " . $_SESSION['time'] . " WHERE id == " . $row[0]);
			echo json_encode("Success");
		}
		else {
			echo json_encode(array("Failed", "Session (database) expired! Please re-login."));
		}
		$DBH = null;
	}
	catch(PDOException $e) {
		echo $e->getMessage();
	}
}
else {
	echo json_encode(array("Failed", "Session (cookie) expired! Please re-login"));
}
?>
