<?php
//Page to create user sessions

include(realpath(dirname(__FILE__)) . "/config.php"); //Pull in $db_path
include(realpath(dirname(__FILE__)) . "/api/create-session.php"); //Pull in DB create function
//Maintenance script call
exec("php api/maintenance.php");

session_start();

//Check if connecting computer is Instructor station
//If so, create/update cookie and DB entry, and forward to control.php
if($_SERVER['REMOTE_ADDR'] == $InstructorStationIP && $enableInstructorStation == true) {
	//Check if session already in DB by IP
	try {
		$DBH = new PDO("sqlite:$db_path");
		if($debugging == true)
			$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging
		$query = $DBH->prepare("SELECT * FROM session WHERE ip = :ip");
		$query->bindParam(':ip', $InstructorStationIP);
		$query->execute();
		$row = $query->fetch(PDO::FETCH_NUM);
		if($row > 0) {
			//Session exists, Match Cookie to DB
                        $_SESSION['user'] = $row[1];
                        $_SESSION['ip'] = $row[4];
                        $_SESSION['time'] = $row[2];
			$_SESSION['sessionkey'] = $row[3];
		}
		else {
			//Session doesn't exist, create DB and Cookie session
			session_destroy(); //Destroy any session that exists
			session_start(); //Create a new session for the instructor
			$_SESSION['user'] = "Instructor Station";
			$_SESSION['ip'] = $InstructorStationIP;
			$_SESSION['time'] = time();
			$_SESSION['sessionkey'] = createSession($db_path, $debugging, $_SESSION['user'], $_SESSION['ip'], $_SESSION['time'], $salt);
		}
		//Forward to control.php
		header('Location: ./control.php');
		die();
	}
        catch(PDOException $e) {
                echo $e->getMessage();
        }
	header('Location: ./control.php');
}


//Check if session already exists (both Cookie and in DB)
if(isset($_SESSION['sessionkey'])) {
	try {
		$DBH = new PDO("sqlite:$db_path");
		$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging
		$query = $DBH->prepare("SELECT * FROM session WHERE sessionkey = :sessionkey");
		$query->bindParam(':sessionkey', $_SESSION['sessionkey']);
		$query->execute();
		$row = $query->fetch(PDO::FETCH_NUM);
		if($row > 0) {
			//Session exists, forward to control page
			header('Location: ./control.php');
		}
		else {
			//Sessionkey not in DB, has a cookie, but not Instructor Workstation.  Delete cookie and force re-login
			session_destroy();
			session_start();
			session_destroy();
		}
		$DBH = null;
	}
	catch(PDOException $e) {
		echo $e->getMessage();
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title>Flipped Classroom TV Collaboration Instructor - <?php echo gethostname(); ?></title>
<!--[if lt IE 9]>
	<script src="./assets/javascripts/html5.js"></script>
<![endif]-->
	<link rel="stylesheet" href="./assets/stylesheets/style.css" />
<!--[if (gt IE 8) | (IEMobile)]><!-->
 	<link rel="stylesheet" href="./assets/stylesheets/unsemantic-grid-responsive.css" />
<!--<![endif]-->
<!--[if (lt IE 9) & (!IEMobile)]>
	<link rel="stylesheet" href="./assets/stylesheets/ie.css" />
<![endif]-->
</head>
<body>
  <div class="grid-container">
    <?php include('header.php'); ?>
    <div class="grid-100">
      <section class="login-block">
      	<form name="login" action="session.php" method="get">
		Name: <input type="text" name="user">
		<input type="submit" value="Enter">
	</form>
      </section>
    </div>
  </div>
<?php include("footer.html"); ?>
</body>
</html>
