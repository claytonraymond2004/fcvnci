<?php
//Cleans up DB removing entries that are too old
header('Content-type: application/json');

//Only allow 1 instance of this script
$fp = fopen('/tmp/fcvnci-maintenance.lock', 'c+');
if(!flock($fp, LOCK_EX | LOCK_NB)) {
        die(json_encode("Script already running! Only 1 instance allowed"));
}

include(realpath(dirname(__FILE__)) . "/../config.php"); //Pull in $timeout_period and $db_path

//Connect to DB and find entires where time difference is > $timeout_period
try {
	$DBH = new PDO("sqlite:$db_path");
	if($debugging == true)
		$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging

	//If Instructor Station is enabled, never remove it from the DB
	if($enableInstructorStation == false) {
		$DBH->exec("DELETE FROM session WHERE (" . time() . "-time) > $timeout_period");
	}
	else {
		$DBH->exec("DELETE FROM session WHERE (" . time() . "-time) > $timeout_period AND user != 'Instructor Station'");
	}
	$DBH = null;
}
catch(PDOException $e) {
	echo $e->getMessage();
}

//Close file lock
fclose($fp);

?>
