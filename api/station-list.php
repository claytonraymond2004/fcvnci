<?php
//Returns a list of all stations
include(realpath(dirname(__FILE__)) . "/../config.php");

session_start();
header('Content-Type: application/json');
if(isset($_SESSION['sessionkey'])) {
        $DBH = new PDO("sqlite:$db_path");
        if($debugging == true)
                $DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging

        //Query stationUsers for information
        $query = $DBH->query("SELECT id, hostname FROM stations ORDER BY hostname ASC");
        $results = $query->fetchALL(PDO::FETCH_ASSOC);
	//Create a "Station" that represents all stations with an id of -1 and preprend to $results
        $all = array("id" => "-1", "hostname" => "All Stations");
	array_unshift($results, $all);
        echo json_encode($results);
}
else {
        echo json_encode("Not Authorized");
}
?>
