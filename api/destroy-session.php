<?php
//Remove a user from the database and destroy their session
include(realpath(dirname(__FILE__)) . "/../config.php"); //Pull in $db_path
//Maintenance script call
exec("php maintenance.php");

header('Content-Type: application/json');

session_start();
if(isset($_SESSION['sessionkey'])) {
	try {
		$DBH = new PDO("sqlite:$db_path");
		if($debugging == true)
			$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging

		//Check if user is currently presenting, if so, kill Remmina
		$query = $DBH->prepare("SELECT * FROM session WHERE sessionkey = :sessionkey AND presenting = 1");
		$query->bindParam(':sessionkey', $_SESSION['sessionkey']);
		$query->execute();
		$row = $query->fetch(PDO::FETCH_NUM);
		if($row > 0) {
			exec("pkill remmina");
		}

		//Remove user from database and show success JSON
		$query = $DBH->prepare("DELETE FROM session WHERE sessionkey = :sessionkey");
		$query->bindParam(':sessionkey', $_SESSION['sessionkey']);
		$query->execute();
		$DBH = null;
		echo json_encode("Success");
	}
	catch(PDOException $e) {
		echo json_encode($e->getMessage());
	}
}
session_destroy();
?>
