<?php
//Update database with Local VNC Server settings
include(realpath(dirname(__FILE__)) . "/../config.php"); //Pull in $db_path

header('Content-Type: application/json');

$port = $_POST['port'];
$password = $_POST['password'];

session_start();
if(isset($_SESSION['sessionkey'])) {
        try {
                $DBH = new PDO("sqlite:$db_path");
                if($debugging == true)
                        $DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging

                //Update user vnc password and port and display success JSON
                $query = $DBH->prepare("UPDATE session SET localvncport = :port, localvncpassword = :password");
                $query->bindParam(':port', $port);
                $query->bindParam(':password', $password);
                $query->execute();
                $DBH = null;
                echo json_encode("Success");
        }
        catch(PDOException $e) {
                echo $e->getMessage();
        }
}
?>
